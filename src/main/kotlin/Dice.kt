data class Dice(var Value: Int, var isLocked: Boolean) {

    constructor() : this(0, false)

    fun roll() {
        if(!this.isLocked) {
            this.Value = (1..6).random()
        }
    }

    fun changeLockedStatus() {
        this.isLocked = !isLocked
    }

    fun reset() {
        this.Value = 0
        this.isLocked = false
    }
}