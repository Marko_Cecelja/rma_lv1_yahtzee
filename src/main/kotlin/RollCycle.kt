import enums.ResultEnum
import java.util.*

class RollCycle(private val dices: MutableList<Dice>) {

    constructor() : this(mutableListOf<Dice>(Dice(), Dice(), Dice(), Dice(), Dice(), Dice()))

    fun start() {
        dices.forEach { dice -> dice.reset() }
        for (i in 1..3) {

            rollDices()
            printDices()

            checkResult()

            if (i != 3) {
                println("Enter 1-6 to lock dice. Enter 0 to finnish!")
                val reader = Scanner(System.`in`)
                var diceIndex: Int = reader.nextInt()

                while (diceIndex != 0) {
                    changeDiceLockedStatus(diceIndex)
                    diceIndex = reader.nextInt()
                }
            }
        }
    }

    private fun rollDices() {
        dices.filter { dice -> !dice.isLocked }.forEach { dice -> dice.roll() }
    }

    private fun printDices() {
        dices.forEach { dice -> println(dice) }
    }

    private fun changeDiceLockedStatus(diceIndex: Int) {
        try {
            dices[diceIndex - 1].changeLockedStatus()
        } catch (e: IndexOutOfBoundsException) {
            println("Provided dice doesn't exist!")
        }
    }

    private fun checkResult() {
        for (result in ResultEnum.values()) {
            if (result.checker.check(dices))
                println(result.title)
        }
    }
}