package checkers

import Dice

class LargeStraightChecker : ResultChecker {

    override fun check(dices: List<Dice>): Boolean {
        val hasOne = dices.stream().anyMatch { dice -> dice.Value == 1 }
        val hasTwo = dices.stream().anyMatch { dice -> dice.Value == 2 }
        val hasThree = dices.stream().anyMatch { dice -> dice.Value == 3 }
        val hasFour = dices.stream().anyMatch { dice -> dice.Value == 4 }
        val hasFive = dices.stream().anyMatch { dice -> dice.Value == 5 }
        val hasSix = dices.stream().anyMatch { dice -> dice.Value == 6 }

        return (hasOne && hasTwo && hasThree && hasFour && hasFive) ||
                (hasTwo && hasThree && hasFour && hasFive && hasSix)
    }
}