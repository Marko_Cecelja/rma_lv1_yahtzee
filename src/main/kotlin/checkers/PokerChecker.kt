package checkers

import Dice

class PokerChecker : ResultChecker {

    override fun check(dices: List<Dice>) : Boolean {
        for (dice in dices) {
            val pokerDices = dices.filter { it.Value == dice.Value }
            if (pokerDices.size == 4) {
                return true
            }
        }
        return false
    }
}