package checkers

import Dice

interface ResultChecker {

    fun check(dices: List<Dice>) : Boolean
}