package checkers

import Dice

class YahtzeeChecker : ResultChecker {

    override fun check(dices: List<Dice>): Boolean {
        for (dice in dices) {
            val yahtzeeDices = dices.filter { it.Value == dice.Value }
            if (yahtzeeDices.size == 5) {
                return true
            }
        }
        return false
    }
}