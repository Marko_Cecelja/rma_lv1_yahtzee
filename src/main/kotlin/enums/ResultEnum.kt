package enums

import checkers.LargeStraightChecker
import checkers.PokerChecker
import checkers.ResultChecker
import checkers.YahtzeeChecker

enum class ResultEnum(val title: String, val checker: ResultChecker) {

    YAHTZEE("Yahtzee", YahtzeeChecker()),
    POKER("Poker", PokerChecker()),
    LARGE_STRAIGHT("Large Straight", LargeStraightChecker())
}