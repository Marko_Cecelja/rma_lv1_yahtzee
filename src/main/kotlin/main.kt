fun main() {

    val rollCycle = RollCycle()

    println("Start new roll cycle? [Y/N]")
    var userInput = readLine() ?: "N"

    while (userInput != "N")  {

        if (userInput != "Y") {
            println("Invalid command! Use 'Y' for Yes and 'N' for No.")
            userInput = readLine() ?: "N"
            continue
        }

        rollCycle.start()
        println("Start new roll cycle? [Y/N]")
        userInput = readLine() ?: "N"
    }
}